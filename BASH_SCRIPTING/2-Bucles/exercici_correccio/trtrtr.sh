#!/bin/sh

MAXIM=100                   
INTENT=0                         
NUM_INTENTS=0                           
NOMBRE=$(( $$ % $MAXIM )           

while [ $INTENT -ne $NOMBRE ]
do
echo -n "Intenta -ho? "
read RESPOSTA
if [ "$INTENT" -lt $NOMBRE ]
then
echo "... més gran!"
elif [ "$INTENT" -gt $NOMBRE ]
then
echo "... més petit!
fi
NUM_INTENTS=$(($NUM_INTENTS + 1))
done
echo "Correcte!! $NOMBRE acertat en $NUM_INTENTS intents."
exit 0
