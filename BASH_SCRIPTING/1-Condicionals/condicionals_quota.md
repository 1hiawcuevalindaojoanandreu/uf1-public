

Seguim amb els scripts *aritmètics*, recordeu que al moodle teníeu les possibles solucions de les sumes i la divisió real fent servir ll'ordre `bc`.

Feu els següents scripts començant per la seva capçalera, descripció i comentaris: 

##### Exercici 1.

Un script com divisio.sh però que mostri 2 decimals. Sisplau abans de fer
l'script:

* `man bc` per trobar l'ordre que posa els decimals a 2 (també podeu buscar a un cercador).
* jugar dintre de `bc` per comprovar si la vostra solució funciona.
* jugar fora de `bc`, a la consola amb `echo` i `bc` fins que surti el que es demana (heu de jugar una estona).
* Un cop resolt a la consola, ja només queda el més fàcil, passar-lo a l'script.

##### Exercici 2.

Jugueu amb `bc` amb expressions relacionals, per exemple:

* 2 < 4
* 2 >= 4
* 3 != 3

deduïu quin és el resultat de `bc` en funció de que l'expressió sigui certa o
no. Un cop fet això, fes un script que rebi una expressió condicional i que
acabi mostrant TRUE o FALSE en funció de que sigui certa o falsa l'expressió
condicional. Hauràs de resoldre un petit problema que pots trobar amb el
redireccionament.

##### Exercici 3.

Fes l'script de quota. Pots fer servir [quota_warning_comentaris.sh](quota_warning_comentaris.sh) que faràs servir d'esquelet per començar a treballar amb ell. Intenta fer els passos (traduir els comentaris a llenguatge bash scripting) a poc a poc.

##### OBSERVACIÓ

Recordeu que amb `$(( ))` també podem fer comparacions condicionals però només amb enters. Per exemple:

```
echo $((7/5 == 4/3)) 
```

dóna TRUE, ja que al fer la divisió entera, la pregunta s'interpreta com a `1 == 1 ?`

mentre que

```
echo "7/5 == 4/3" | bc -l 
```

dóna FALSE, ja que la pregunta s'interpreta com a `1.4  == 1.333 ?`
 
