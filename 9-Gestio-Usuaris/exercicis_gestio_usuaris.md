### Exercicis gestió d'usuaris

*Gestió d'usuaris i grups (LPI 107.1)*

##### Exercici 1

Crea l'usuari *usuari1*.

Per omissió, quin directori personal assigna el sistema a l'usuari?

A on es troba això? (Pensa quina ordre el fa servir i mira la seva ajuda).

Com es pot canviar de manera permanent o temporal?

##### Exercici 2

Elimina la contrasenya de l'usuari *usuari1*

##### Exercici 3

Repeteix les accions anteriors per a *usuari2* i *usuari3*

##### Exercici 4 

Crea el grup *usuaris*.

##### Exercici 5 

Mira quin es el gid de cada usuari?

Un usuari pot pertanyer a més d'un grup?

Quin és(són) el(s) seu(s) grup(s) per defecte?

Si un usuari té més d'un grup, hi ha alguna manera de diferenciar-los
jeràrquicament? (És a dir, en importància)

##### Exercici 6 

Afegeix *usuari1* i *usuari2* al grup *usuaris*.

##### Exercici 7

Quina ordre hauries utilitzat si abans de crear *usuari1* i *usuari2* hagués
existit *usuaris* ?

##### Exercici 8

Que cal fer per tal de que tots els usuaris que es donin d'alta al sistema,
automàticament tinguin les següents característiques (sense tenir que
especificar-les com a paràmetre de adduser)?

* Estructura de directori de l'usuari amb els subdirectoris: projecte (amb
  permisos rwxr-x---), privat (amb permisos rwx------)

* Els directoris que creï l'usuari tinguin els permisos 750 (rwxr-x---)

##### Exercici 9

Un treballador d'una empresa intercanvia fitxers entre l'usuari d'un sistema GNU/Linux que té al
treball i el seu usuari personal GNU/Linux que té al seu ordinador domèstic,
amb logins diferents. Gairebé sempre té problemes amb els permisos. No és el
*root* del treball però sí el de casa seva. Quina solució li proposaries?

##### Exercici 10

Com a administradors d'un sistema, ens comuniquen que un cert usuari faltarà a
la feina durant un temps indeterminat i no volem que ningú pugui accedir al seu
compte. Quina solució proposaries?

